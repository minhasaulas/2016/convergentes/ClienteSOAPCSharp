﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteSOAPCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            WSSOAP.WSClient cliente = new WSSOAP.WSClient();
            double resultado = cliente.somar(10, 20);
            Console.WriteLine("O resultado é: " + resultado);
            Console.Read(); //pausa
        }
    }
}
